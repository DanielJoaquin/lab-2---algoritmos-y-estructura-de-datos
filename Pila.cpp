#include <iostream>
#include "Pila.h"

using namespace std;

/*Constructor*/
Pila::Pila(int max){
  this->max = max - 1;
  this->pila = new int[max];
}

/*Metodos*/
void Pila::pilaLlena(){/*Verificador si la pila esta o no llena*/
  if(this->tope == max){
    this->band = true;
  }
  else{
    this->band = false;
  }
}

void Pila::pilaVacia(){ /*Verificador si la pila esta o no vacia*/
  if(this->tope == -1){
    this->band = true;
  }
  else{
    this->band = false;
  }
}

void Pila::push(int numero){ /*Funcion para agregar numeros a la pila*/
  pilaLlena();
  if(this->band == false){
    this->tope = tope + 1;
    pila[tope] = numero;
    cout << "Valor ingresado a la Pila: " << numero << endl;
  }
  else{
    cout << "PILA LLENA!!, no se pueden agregar mas numeros" << endl;
  }
}

void Pila::pop(){ /*Funcion para eliminar numeros de la pila*/
  pilaVacia();
  if(this->band == false){
    cout << "Valor eliminado de la Pila: " << pila[tope] << endl;
    this->tope = this->tope -1;
  }
  else{
    cout << "PILA VACIA!!, no se pueden quitar numeros" << endl;
  }
}

void Pila::imprimirPila(){ /*Funcion para Imprimir la Pila*/
  pilaVacia();
  if((this->band) == false){
    cout << "\n" << endl;
    for(int i=(this->tope); i>=0; i--){
      cout << "|" << this->pila[i] << "|" << endl;
    }
  }
  else{
    cout << "PILA VACIA!!" << endl;
  }
}
