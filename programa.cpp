#include <iostream>
#include "Pila.h"

using namespace std;

/*Menu del programa*/
int Menu(){
  string opcion;
  cout << "\n-------Menu de Opciones-------" << endl;
  cout << "|[1] Agregar numero / push   |" << endl;
  cout << "|[2] Remover numero / pop    |" << endl;
  cout << "|[3] Ver Pila                |" << endl;
  cout << "|[0] Salir                   |" << endl;
  cout << "------------------------------" << endl;
  cout << "\nIngrese Opcion: ";
  getline(cin, opcion);
  return stoi(opcion);
}

int main(){

  /*Variables*/
  string line;
  int N;
  int opcion = -1;
  int numero;

  /*Definir el tamaño de la PIla*/
  cout << "Ingrese el maximo de la pila: ";
  getline(cin, line);
  N = stoi(line);
  Pila p = Pila(N);

  /*Acciones del Programa*/
  while(opcion!=0){
    opcion = Menu();

    switch (opcion) {
      case 0:
        cout << "PROGRAMA TERMINADO!" << endl;
        break;
      case 1:
        cout << "Ingrese un numero: ";
        getline(cin, line);
        numero = stoi(line);
        p.push(numero);
        break;
      case 2:
        p.pop();
        break;
      case 3:
        p.imprimirPila();
        break;

    }
  }














  return 0;
}
