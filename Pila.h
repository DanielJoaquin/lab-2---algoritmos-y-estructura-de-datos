#include <iostream>

using namespace std;

#ifndef PILA_H
#define PILA_H


class Pila{

  private:
    int max = 0;
    int tope = -1;
    bool band;

    int *pila = NULL;

  public:
    /*Constructor*/
    Pila(int max);
    /*Metodos*/
    void pilaLlena(); /*Verificador si la pila esta o no llena*/
    void pilaVacia(); /*Verificador si la pila esta o no vacia*/
    void push(int numero); /*Funcion para agregar numeros a la pila*/
    void pop(); /*Funcion para eliminar numeros de la pila*/
    void imprimirPila(); /*Funcion para Imprimir la Pila*/

};
#endif
